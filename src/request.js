module.exports = function( context ) {
  let workspaceId = JSON.parse( localStorage.getItem( 'insomnia::meta::activeWorkspaceId' ) )
  let tokenVarName = context.request.getEnvironmentVariable( 'tokenVarName' ) || 'token'
  let storageName = workspaceId + tokenVarName
  let token = localStorage.getItem( storageName )
  if ( token )
    context.request.setHeader( 'Authorization', token )
}

